import java.io.File
import java.time.LocalDate
import kotlin.io.path.Path
import kotlin.io.path.createDirectory
import kotlin.io.path.notExists

/*
    Application: Wordle 2022

    Description: Wordle es un juego de adivinar palabras, en este caso se propone una palabra oculta de 5 letras.
    En él, tienes que adivinar una palabra en seis intentos, en los que no se te dan más pistas que decirte
    qué letras de las que has puesto están dentro de la palabra. El objetivo es descubrir la palabra oculta,
    trabajando la lógica y la rapidez mental.

    Options: Cada vez que realizas un intento tienes que escribir una palabra válida, y se te dirá cuáles
    de las letras introducidas están y cuáles no, y si están en el sitio donde les toca o no.
        1. Si la letra no existe dentro de la palabra se marcará en GRIS
        2. Si la letra existe dentro de la palabra, pero no está en el lugar que le corresponde se marcará en AMARILLO
        3. Si la letra existe y está en el sitio donde le corresponde se marcará en VERDE

    Author: Sergio González
    Date: 2022, November 12th
 ---------------------------------------------------------------------------------------------------------------------------
    Create functions, tests & documentation
    Author: Sergio González
    Deadline: 2023, January 8th
 ---------------------------------------------------------------------------------------------------------------------------
    Create, load files & paths
    Author: Sergio González
    Deadline: 2023, February 19th

 */

// VARIABLE GLOBALES
// Ahora y en el futuro se declararán aquí las variables globales
// Se utilizarán para crear funciones y realizar posibles tests

// Cargamos el idioma previo con el que se jugarán las palabras
var idioma = "esp"

// Variable que contendrá los diferentes diccionarios: esp, cat y eng
//var diccionario = arrayListOf<String>()

// Prueba de diccionario usado para la prueba del juego
var diccionario = arrayListOf("pacos", "paces", "pacas","pacis")

/* Se declaran los colores que se usaran dentro del juego
El prefijo 48 se usará para los fondos y 38 para las letras */
val green = "\u001b[48;2;0;204;0m"
val verde = "\u001b[38;2;0;204;0m"
val yellow = "\u001b[48;2;240;234;39m"
val grey = "\u001b[48;2;160;160;160m"
val black = "\u001b[38;2;0;0;0m"
var white = "\u001b[48;2;255;255;255m"
var blanco = "\u001b[38;2;255;255;255m"
val orange = "\u001b[48;2;255;87;51m"
val naranja = "\u001b[38;2;255;87;51m"

// Usamos la variable noPintar para no colorear los caracteres o líneas siguientes
val noPintar = "\u001b[m"

// Declaramos nuevas variables ampliación previa
// Variables sobre las partidas
var cantidadPartidasGanadas = 0
var cantidadPartidasNoGanadas = 0

// Variables sobre Intentos y Rachas
var mejorRachaIntentosActual = 0
var cantidadIntentosAcertar = 0
var mejorRachaIntentosTotal = 0
var medianaIntentos: Double = 0.0

// Variable sobre porcentajes y cantidad de partidas
var porcentajePartidasGanadas = 0.0


fun main(){

    println("-------------------------------")
    println("[̲̅w̲̅][̲̅o̲̅][̲̅r̲̅][̲̅d̲̅][̲̅l̲̅][̲̅e̲̅]")
    println("-------------------------------")
    println("||    BIENVENIDO A WORDLE    ||")
    println("-------------------------------")

    // Pedimos el nombre del usuario que vamos a crear
    // o el nombre del usuario ya registrado que continuará con sus partidas
    println("Introduce tu nombre de jugador:")
    val jugadorWordle = readln().replace(" ","_")
    menuJuego()
    guardarEstadisticas(jugadorWordle)

}

/**
 * Se crea la función que contiene el juego Wordle y comienza la partida
 */

fun jugarPartida() {

    /* Declaración de variables */
    // Volver a jugar
    var volverJugar: String
    do {

        // Generamos la palabra que hay que adivinar
        val palabraRandom = diccionario.random()
        /* Y generamos las variables de por donde empezar a contar el contenedor de cada palabra [0] del array creado */
        // Hemos cambiado redundancia con anterior versión
        var posicionLetraPalabra: Int

        // Y el tipo de variable para jugar la cual será la palabra que el jugador introducirá
        var palabraIntroducida: String

        // Variable de los intentos del jugador igual a 6 con inicio en 0
        var intentos = 0

        /*
        CHIVATO
        Uso de chivato para comprobar el código.
        Se usa este color para indicar que se ha generado la palabra
        Este chivato muestra toda la palabra
        println("$white$black $palabraRandom $noPintar\n")
        Este chivato está tapado por color blanco ¡No usar el mouse por encima, eso es trampa!
        println(white + white + " $palabraRandom " + noPintar + "\n")
        */

        // Iniciamos el juego
        do {

            println("|> Adivina la palabra aleatoria")
            println("|> Tienes 6 intentos")
            println("|> ¡EMPEZAMOS!")
            println("-------------------------------")
            println(" Introduce la palabra: ")

            // Espacio de memoria donde se introduce y guarda la palabra del jugador
            palabraIntroducida = readLine()!!.toString().lowercase()

            // Se reinicia la posición de inicio de cada palabra
            posicionLetraPalabra = 0

            // Generamos un if/else para indicar que la palabra debe ser de 5 letras exclusivamente y si se contiene en el diccionario
            // Añadir para hacer que las palabras introducidas pertenezcan al diccionario --> && diccionario.contains(palabraIntroducida)
            if (palabraIntroducida.length == 5) {

                // Dentro de un for creamos un if/ else if /else que contendrá el juego en sí
                // Se ha intentado: hay momentos en que el trabajo de los colores con las letras funciona y otras no,
                val letrasRepetidas = arrayListOf<Char>()

                for (i in palabraIntroducida) {
                    // Si la palabra aleatoria contiene las letras y es igual a la palabra se coloreará en verde
                    if (palabraRandom[posicionLetraPalabra] == i) {
                        print(green + black + i + noPintar)
                        letrasRepetidas.add(i)
                        // Si contiene la letra, pero en lugar diferente al que le toca se coloreará en amarillo
                    } else if (palabraRandom.contains(i)) {
                        print(yellow + black + i + noPintar)
                        letrasRepetidas.add(i)
                        // En caso de que la letra no este en la palabra se coloreara de gris
                    } else {
                        print(grey + black + i + noPintar)
                    }
                    /* Las letras de las palabras se iran leyendo y sumando la posición
                    a cada ronda que el jugador vaya introduciendo una nueva palabra para
                    así ir comparándola con la palabra random generada */
                    posicionLetraPalabra++
                }
                /* Aumentamos la cantidad de intentos que tendrá el jugador hasta 6 que será el máximo,
                 teniendo 0 intentos, desde el principio, que será el mínimo */
                intentos++

                print("\nEste es tú $naranja$intentos$noPintar intento/s \n")
            } else {

                /* Se comprueba si la palabra introducida tiene 5 letras como en el diccionario introducido,
                si añadimos la parte de código en la que la palabra debe perteneces al diccionario habrá que modificar la frase.
                Y se indica que introduzca una palabra de 5 letras */
                println("$white$orange La palabra introducida no es de 5 letras. " +
                        "$noPintar\nIntroduce una palabra de 5 letras, por favor")
            }

        } while (intentos < 6 && palabraIntroducida != palabraRandom)

        if (palabraIntroducida == palabraRandom) {
            // En caso de que la palabra que haya introducido la palabra correcta ganará
            println("$green$black FELICIDADES! $noPintar Has acertado")

            // Suma para el cálculo final de las estadísticas
            cantidadPartidasGanadas++
            cantidadIntentosAcertar += intentos
            porcentajePartidasGanadas = cantidadPartidasGanadas.toDouble() / (cantidadPartidasGanadas + cantidadPartidasNoGanadas) * 100
            mejorRachaIntentosActual++

        } else {
            /* Se le indica al jugador que no ha acertado si cumple la condición de sí el número de intentos es = a 0
            o < de 0 al mismo tiempo que la palabra random es diferente a la palabra que ha introducido */
            print("Vaya no has acertado la palabra. ")
            print("La palabra era $naranja$palabraRandom $noPintar\n")

            cantidadPartidasNoGanadas++
            // Se guarda la mejor racha actual en vez de la que ya era actual
            if (mejorRachaIntentosTotal < mejorRachaIntentosActual) mejorRachaIntentosTotal =
                mejorRachaIntentosActual
            // Reiniciamos la racha actual, ya que se ha perdido la partida
            mejorRachaIntentosActual = 0
        }
        // Una vez realizado el juego, la palabra random usada sea borrada del diccionario
        diccionario.remove(palabraRandom)

        // Calculamos la mediana de intentos necesarios
        medianaIntentos = cantidadIntentosAcertar.toDouble() / cantidadPartidasGanadas
        if (cantidadPartidasGanadas == 0) medianaIntentos = 0.0
        val cantidadPartidasJugadas = cantidadPartidasGanadas + cantidadPartidasNoGanadas

        // Llamamos a la función para generar las estadísticas
        mostrarEstadisticas(white, black, verde, naranja, blanco, noPintar, cantidadPartidasJugadas, cantidadPartidasGanadas, cantidadPartidasNoGanadas, porcentajePartidasGanadas, medianaIntentos,
            mejorRachaIntentosActual, mejorRachaIntentosTotal)

        println("\nEl juego ha terminado. Quieres seguir ejercitando tu cerebro? $white$black y/n $noPintar")
        volverJugar = readLine()!!.toString()

        // Se indica que para volver a jugar debe ser igual a la letra "y" siempre que el diccionario no esté vacío
    } while (volverJugar == "y" && diccionario.isNotEmpty())

    // Si el diccionario esta vacío se imprime la siguiente frase
    if(diccionario.isEmpty()) println("Gracias por jugar a $white$black WORDLE $noPintar Se han terminado las palabras. Te esperamos pronto \n")
    // Anteriormente al no haber indicado que quería seguir jugando se le da la despedida
    else println("Gracias por jugar a $white$black WORDLE $noPintar Te esperamos pronto")
}


/**
 * Se crea una función que mostrar las estadísticas del juego
 * @param white String que contiene el código de color blanco que se imprime en el terminal.
 * @param black String que contiene el código de color negro que se imprime en el terminal.
 * @param verde String que contiene el código de color verde que se imprime en el terminal.
 * @param naranja String que contiene el código de color naranja que se imprime en el terminal.
 * @param blanco String que contiene el código de color blanco de fondo que se imprime en el terminal
 * @param noPintar String que contiene el código de no pintar los Strings/Integer en el terminal.
 * @param cantidadPartidasJugadas Integer que representa la cantidad de partidas jugadas.
 * @param cantidadPartidasGanadas Integer que representa la cantidad de partidas ganadas.
 * @param cantidadPartidasNoGanadas Integer que representa la cantidad de partidas no ganadas.
 * @param porcentajePartidasGanadas Número del tipo Double que representa el porcentaje de partidas ganadas.
 * @param medianaIntentos Número del tipo Double que representa la mediana de intentos.
 * @param mejorRachaIntentosActual Integer que representa la mejor racha actual de intentos.
 * @param mejorRachaIntentosTotal Integer que representa la mejor racha total de intentos.
 * @return La función imprime las Estadísticas generadas sobre el juego con los diferentes parámetros indicados.
 */

fun mostrarEstadisticas(white: String,
                        black: String,
                        verde: String,
                        naranja: String,
                        blanco: String,
                        noPintar: String,
                        cantidadPartidasJugadas: Int,
                        cantidadPartidasGanadas: Int,
                        cantidadPartidasNoGanadas: Int,
                        porcentajePartidasGanadas: Double,
                        medianaIntentos: Double,
                        mejorRachaIntentosActual: Int,
                        mejorRachaIntentosTotal: Int) {

    // Creamos estadísticas de las partidas
    println("\n-----------------$white$black ESTADÍSTICAS WORDLE $noPintar-----------------\n")
    // Se imprimen las partidas ganadas y no ganadas
    println("Partidas jugadas: $white$black $cantidadPartidasJugadas $noPintar")
    println(".........................")
    // Se imprimen las partidas ganadas y no ganadas
    println("Partidas ganadas:$verde $cantidadPartidasGanadas $noPintar")
    println("Partidas perdidas:$naranja $cantidadPartidasNoGanadas $noPintar")
    println(".........................")
    // A partir del calculo de las partidas ganadas y no ganadas imprimimos el %
    println("Porcentaje de partidas ganadas:$verde $porcentajePartidasGanadas% $noPintar")
    // Calculamos la media de intentos, sera NaN hasta que no tenga número con los que calcular la mediana
    println("Mediana de intentos: " + blanco + String.format("%.2f", medianaIntentos) + noPintar)
    println(".........................")
    // Imprimimos la mejor racha actual y total. Serán 0 hasta que tengan intentos con los que calcular los datos
    println("Mejor racha actual:$blanco $mejorRachaIntentosActual $noPintar")
    println("Mejor racha total:$blanco $mejorRachaIntentosTotal $noPintar\n")
    println("-----------------$white                     $noPintar-----------------")

}

/**
 * Se crea una función que muestra el menú de bienvenida al juego Wordle.
 * Se imprime el menú en la terminal para comenzar el juego
 */
fun menuJuego() {

    // Inicializamos el juego con un menú de bienvenida
    println("-------------------------------")
    println("[̲̅w̲̅][̲̅o̲̅][̲̅r̲̅][̲̅d̲̅][̲̅l̲̅][̲̅e̲̅]")
    println("-------------------------------")
    println("||    BIENVENIDO A WORDLE    ||")
    println("-------------------------------")

    // El menú de decisión para utilizar las funciones correspondientes
    println("|> MENÚ JUEGO")
    // Iniciamos el idioma del diccionario, en este caso el español
    println("|> 1. Cambiar idioma. Idioma cargado: $idioma")
    println("|> 2. Ver estadisticas")
    println("|> 3. JUGAR")
    when (readln()) {
        "1" -> {
            cambiarIdioma()
        }
        "2" -> {
            nombreJugador()
        }
        "3" -> {
            jugarPartida()
        }
        else -> println("Algunos no hablamos Klingon, qatlho'")
    }
}

/**
 * Función de cálculo de porcentaje de Partidas Ganadas para hacer test
 */
fun porcentajeDePartidasGanadas(cantidadPartidasGanadas: Int, cantidadPartidasNoGanadas: Int): Double {
    porcentajePartidasGanadas = cantidadPartidasGanadas.toDouble() / (cantidadPartidasGanadas + cantidadPartidasNoGanadas) * 100
    return porcentajePartidasGanadas
}

/**
 * Función para que el usuario cambie de idioma
 */
fun cambiarIdioma() {
    // Menú para cambiar idioma
    println("Escoge idioma con el que jugar?")
    println("1. Español")
    println("2. Català")
    println("3. English")
    // Idiomas a los que puede acceder el usuario/jugador
    when (readln()) {
        "1" -> {
            cargarDiccionario("esp")
            idioma = "esp"
        }
        "2" -> {
            cargarDiccionario("cat")
            idioma = "cat"
        }
        "3" -> {
            cargarDiccionario("eng")
            idioma = "eng"
        }
        else -> println("Algunos no hablamos Klingon, qatlho'")
    }
    return menuJuego()
}

fun cargarDiccionario(idioma: String) {
    // Necesitamos que el diccionario esté vacio
    diccionario = arrayListOf()
    // Escogemos el diccionario seleccionado por el jugador
    val fitxer = File("./idioma/$idioma.txt")
    // Rellenamos el diccionario con el idioma escogido por el jugador
    fitxer.forEachLine { diccionario.add(it) }
}

/**
 * Función que guarda las estadísticas del jugador
 * @param jugadorWordle Nos devuelve el nombre del usuario
 */
fun guardarEstadisticas(jugadorWordle: String){
    // Si no existe se crea la carpeta estadísticas
    val dir = Path("./estadisticas/")
    if (dir.notExists()){ dir.createDirectory() }
    // Si no existe el fichero del jugador se crea
    val fitxer = File("./estadisticas/$jugadorWordle.txt")
    if (!fitxer.exists()){fitxer.createNewFile()}
    // Cogemos la fecha actual
    val fecha = LocalDate.now()
    // Escribimos los datos escogidos para rellenar el archivo de las estadísticas del jugador
    val cantidadPartidasJugadas = cantidadPartidasGanadas + cantidadPartidasNoGanadas
    fitxer.appendText("$fecha;$cantidadPartidasJugadas;$cantidadPartidasGanadas;$cantidadPartidasNoGanadas;$porcentajePartidasGanadas;$medianaIntentos;$mejorRachaIntentosTotal\n")
}

/**
 * Esta función muestra las estadísticas del jugador que estamos pidiendo en el menú
 * @param jugadorWordle Pedimos el nombre del jugador del que necesitamos las estadísticas
 */
fun estadisticasJugador(jugadorWordle: String) {
    // Una vez creado en el archivo cogemos el archivo del jugador
    val archivo = File("./estadisticas/$jugadorWordle.txt")
    // Iteramos con cada línea del archivo para operar con la información
    archivo.forEachLine {
        // Por cada dato en concreto lo separamos y hacemos un split transformándolo en un .toInt o .toDouble
        val puntuacion = it.split(";")
        val fecha = puntuacion[0]
        val cantidadPartidasJugadas = puntuacion[1].toInt()
        val cantidadPartidasGanadas = puntuacion[2].toInt()
        val cantidadPartidasNoGanadas = puntuacion[3].toInt()
        val porcentajePartidasGanadas = puntuacion[4].toDouble()
        val medianaIntentos = puntuacion[5].toDouble()
        val mejorRachaIntentosTotal = puntuacion[6].toInt()

        // Enseñamos las estadísticas
        println("\n-----------------$white$black ESTADÍSTICAS WORDLE $noPintar-----------------")
        println("\n-----------------$white$black $jugadorWordle $noPintar-----------------\n")
        // Se imprimen la fecha y las partidas jugadas
        println("Fecha de partida: $fecha")
        println("Partidas jugadas: $white$black $cantidadPartidasJugadas $noPintar")
        println(".........................")
        // Se imprimen las partidas ganadas y no ganadas
        println("Partidas ganadas:$verde $cantidadPartidasGanadas $noPintar")
        println("Partidas perdidas:$naranja $cantidadPartidasNoGanadas $noPintar")
        println(".........................")
        // A partir del calculo de las partidas ganadas y no ganadas imprimimos el %
        println("Porcentaje de partidas ganadas:$verde $porcentajePartidasGanadas% $noPintar")
        // Calculamos la media de intentos, sera NaN hasta que no tenga número con los que calcular la mediana
        println("Mediana de intentos: " + blanco + String.format("%.2f", medianaIntentos) + noPintar)
        println(".........................")
        // Imprimimos la mejor racha total. Serán 0 hasta que tengan intentos con los que calcular los datos
        println("Mejor racha total:$blanco $mejorRachaIntentosTotal $noPintar")
        println("\n-----------------$white$black       $noPintar-----------------\n")
        println("-----------------$white                     $noPintar-----------------")

    }
}

/**
 * Creación de jugador para la base de datos y llamar a las estadísticas del jugador para poder mostrárselas
 */
fun nombreJugador() {
// Para ver las estadísticas de un usuario le pedimos el nombre
    println("Introduce el nombre del jugador para mostrar sus estadisticas:")
    val jugadorWordle = readln()
    // Comprobamos si el usuario existe, si el usuario existe mostramos las estadísticas,
    // si no existe le mostramos el mensaje de que no existe
    val fitxer = File("./estadisticas/$jugadorWordle.txt")
    if (fitxer.exists()){
        estadisticasJugador(jugadorWordle)
    } else {
        println("No hay datos registrados de este jugador.")
    }
}





