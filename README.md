# CREACIÓN DE WORDLE EN KOTLIN

- [Que es Wordle?](#que-es-wordle)
- [Análisis](#analisis)
- [Programación](#programacion)
- [Conclusiones previas](#conclusiones)
- [Ampliación](#ampliacion)
- [Funciones](#funciones)
- [Tests](#tests)
- [Documentación](#documentacion)
- [Archivos](#archivos)
- [Conclusiones finales](#conclusionesfinales)

## Que es Wordle? <a name="que-es-wordle"></a>
<span style="Color: blue">Wordle</span> es un juego de adivinar palabras, una palabra por día, que tiene un formato de crucigrama y con similitudes con otros juegos como el Mastermind. En él, tienes que adivinar una palabra en seis intentos, en los que no se te dan más pistas que decirte qué letras de las que has puesto están dentro de la palabra.

## Que es Wordle? <a name="que-es-wordle"></a>

<span style="Color: blue">Wordle</span> es un juego de adivinar palabras, una palabra por día, que tiene un formato de crucigrama y con similitudes con otros juegos como el Mastermind.
En él, tienes que adivinar una palabra en seis intentos, en los que no se te dan más pistas que decirte qué letras de las que has puesto están dentro de la palabra.


## Análisis <a name="analisis"></a>

##### El análisis es lo primero que había que hacer sobre el juego Wordle ORIGINAL del cual se extrajeron los siguientes puntos o informaciones:
    - El juego consiste en adivinar 1 palabra de 5 letras por día
    - Hay que adivinar la palabra oculta en el menor tiempo posible
    - El jugador solo puede usar otras palabras de 5 letras para adivinar la palabra oculta
    - El jugador tiene 6 intentos para conseguirlo
    - Cada vez que realizas un intento tienes que escribir una palabra válida
    - Si no consigues adivinar la palabra se termina el juego por hoy


##### En 6 Intentos veremos:
    1. Si la letra no existe dentro de la palabra, la letra se coloreará en GRIS
    2. Si la letra existe dentro de la palabra pero no esta en el lugar que le corresponde, la letra se coloreará en AMARILLO
    3. Si la letra existe y está en el sitio donde le corresponde se coloreará en VERDE
    4. Si una palabra contiene 2 letras iguales pero:
        - Una se colorea en VERDE esta en el sitio correspondiente y si se colorea en AMARILLO no esta en el sitio correspondiente
        - Las 2 letras iguales se colorean en AMARILLO no estan en el sitio correspondiente
    5. Si la palabra es correcta, la palabra se coloreará totalmente en VERDE y el juego terminará
    6. Lo rápido e inteligente que eres :P


## Programación <a name="programación"></a>

La fase de programación ha sido la más complicada y la que más tiempo y repetición ha llevado, la estructura se ha intentado definir lo más limpia y estructurada posible, valga la redundancia.
En el propio documento se ha intentado comentar cada fase de código para que a la vez que se lea se pueda entender el porqué se ha hecho eso y para qué sirve esa parte de código, algo que a mí también me ha servido para ir implementando
poco a poco e ir generando el archivo y al mismo tiempo entender el porqué y que realizan esas líneas de código y sobre todo gracias a realizar un paso previo a través del análisis en formato físico.


## Conclusiones previas<a name="conclusiones"></a>

Sinceramente, pese a haber conseguido una versión muy básica (por llamarlo de alguna manera) y viable del programa no estoy nada satisfecho con mi trabajo, ya que para mí ha sido un mundo de necesidades
y preguntas a fuentes externas para poder llegar hasta aquí y a pesar de que una vez finalizado y parándome a leer el código soy capaz de entenderlo, volver a crear algo de esta "magnitud" para mí sería otro desafío,
el cual me gustaría volver a intentar, pero cosa que no quitaría el desespero. La verdad es que gracias a este trabajo he podido aprender más a trabajar con los <arrays> y sobre todo con la implementación de colores
tanto en letras como en fondos dentro de Kotlin, así como practicar más con las condiciones que ya propone en sí la programación.

Si en un futuro se pudiera mejorar esta aplicación sería un reto, tanto en mejora de código como en un futuro visual.


## Ampliación y mejora <a name="ampliacion"></a>

Se amplía el proyecto WORDLE y se mejoran algunos de sus aspectos, como se describen

##### Mejoras:
    - Revisión y limpieza de código
    - Mejora de nombres de variables
    - Mejora para rápidez del compilador

##### Ampliaciones
    Se han añadido las siguientes ampliaciones:
     - Una vez utilizada una palabra aleatoria esta se borra del array para que no se pueda repetir
     - Se añaden ESTADÍSTICAS de juego

###### ¿Qué se añade a las ESTADÍSTICAS?
    - El número de partidas totales
    - El número de partidas ganadas y perdidas
    - El porcentaje de partidas ganas y la media de intentos necesario para ello
    - Y tanto la mejor racha actual como la mejor racha total de juego

![Estadísticas](Estadisticas.PNG)

##### Nuevas Ampliaciones
    Se han añadido las nuevas ampliaciones:
    - Revisión y limpieza de código y comentarios
    - Mejora de nombres de variables
    - Mejora para rápidez del compilador
    - Se añaden funciones de código
    - Realización de Tests de funciones
    - Realización de Documentación

##### Código
    - Para acceder al archivo Main.kt
        · Acceder al directorio gonzalezSergio_Wordle_Documentacion\src\main\kotlin\Main.kt

## Funciones<a name="funciones"></a>

Se realizan mejoras en el proyecto WORDLE añadiendo funciones

##### Mejoras:
    - Se añaden funciones
        · función menuJuego > En esta función se añade el menú principal del juego
        · función generarEstadisticas > En esta función se añade la creación de estadísticas mediante una función 

    - Funciones
        · Revisar para la creación de futuras funciones si es posible


#### Conclusión Funciones:

Al intentar realizar las funciones dentro del código, o al menos al intentar ubicarlas, solo se han podido realizar las actuales funciones debido a la falta de entendimiento de ellas y a la complicación que genera el cambio de código y que habría que generar un nuevo proyecto.


## Tests<a name="tests"></a>

Realización de un test de comprobación de porcentaje (%) de partidas ganadas y partidas no ganadas durante el juego.

##### Realización de test:
    - Para acceder al test realizado acceder al archivo MainKtTest.kt
        · Acceder al directorio gonzalezSergio_Wordle_Documentacion\src\test\kotlin\MainKtTest.kt
    - Para acceder sumario de realización de tests se pueden visualizar en el archivo index.html
        · Acceder al directorio gonzalezSergio_Wordle_Documentacion\build\reports\tests\test\index.html

### Comprobación del % de partidas ganadas
#### Ejemplo de tests realizados:

##### Expected:
![Test Positivo](TestOk.PNG)

##### Unexpected:
![Test Negativo](TestNeg.PNG)

## Documentación<a name="documentacion"></a>

Realización del archivo Dokka Html para visualizar el árbol de la programación del programa Wordle.
##### Realización del archivo:
    - Para acceder a la documentación realizada acceder al archivo index.html
        · Acceder al directorio gonzalezSergio_Wordle_Documentacion/build/dokka/index.html
![Dokka](Dokka.PNG)

## Archivos<a name="archivos"></a>

Realización del archivo Dokka Html para visualizar el árbol de la programación del programa Wordle.
##### Realización de archivos de control:
    - Se realiza la creación de archivos de control de usuarios y partidas
        - Usuarios: se crea archivo de base de datos de usuarios 
        - Partidas: se crea una bae de datos de partidas para crear estadísticas de los usuarios

## Conclusiones finales<a name="conclusionesfinales"></a>

Tal y como se comentó en las conclusiones previas, el haber generado una aplicación de código de una forma muy básica ha hecho que la realización de esta segunda parte en la que
se han generado funciones a partir del código, tests a partir de los mismos y documentación de nuestro trabajo ha hecho que la realización de todo ello haya quedado un poco diluida.
También es cierto que mediante el poco trabajo, por la cantidad que el propio código nos ha brindado y el cual se ha intentado realizar con funciones sin ningún éxito, que se ha podido
realizar nos ha hecho ver la importancia de realizar el código lo más ordenado y limpio posible para tal de poder realizar posibles tests y la documentación necesaria para la
presentación en un futuro de nuestras siguientes aplicaciones.

Sea como fuere, en un futuro sería interesante volver a crear este código de 0 y desde una perspectiva en la que se piensen en funciones y tests a realizar.

------------------------------------------------------------------
Realizado por Sergio González